# Pan-tilt Ultrasound Mid - Air Haptics

Welcome to the **PUMAH** Gitlab repository. PUMAH is a device extending the workspace of **ultrasound arrays** up to 14x their initial size. The array, mounted on a frame, is rotating with **two servo-motors**. The tracking of the target is made through **steamVR** with a [Vive Tracker](https://www.vive.com/fr/vive-tracker/).  We used the [Ultrahaptics STRATOS Explore](https://www.ultraleap.com/product/stratos-explore/) as the ultrasound array.

### Assembly

Pumah is made up of ~20 printed parts assembled with aluminum tubes. You can print the parts yourself (.stl models are in CAO folder), and purchase everything needed for less than 150€.

Follow the instructions in the [documentation ](./PUMAH_documentation.pdf) to assemble your own device.

### Mechanical Calibration

Once assembled, you need to calibrate your servo-motors and record the matching PWM values for given angles. The servo-motors are driven by a USB servo controller called [Pololu Maestro](https://www.pololu.com/category/102/maestro-usb-servo-controllers).

> You can implement your own classes if you've got a different board, or if you want to change the rotation mechanics.

Every step of the calibration process is described in the [documentation ](./PUMAH_documentation.pdf).


### Software Calibration

In this repository you'll find a Unity project containing a calibration scene for the PUMAH.
The scene uses SteamVR and should be compatible with all commercial HMD, but you'll need a Vive Tracker to calibrate the device as-is.

> If you wish to calibrate the device with another method, feel free to change the calibration script called CalibControl that you'll find on SteamVR controllers in the scene.

Follow the instructions in the [documentation ](./PUMAH_documentation.pdf) to locate your device in space and match it with the model position in your virtual environment.


## Documentation

You'll find the [PUMAH_documentation.pdf](./PUMAH_documentation.pdf) here.

### Classes diagram

![Diagramme_PUMAH](./PUMAH_diagram.png)

## Licencing
![88x31](./cc.png)

PUMAH's Hardware (c) by Thomas Howard is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.

You should have received a copy of the license along with this work.  If not, see it [here](http://creativecommons.org/licenses/by-sa/3.0/).

![gplv3-127x51](./gpl.png)

PUMAH is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License. PUMAH is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with PUMAH.  If not, see it [here](https://www.gnu.org/licenses/gpl-3.0.en.html).