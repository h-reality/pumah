﻿using System;
using UnityEngine;

/**
    05/03/2020 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    This file is part of PUMAH.

    PUMAH is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PUMAH is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PUMAH.  If not, see <https://www.gnu.org/licenses/>.

    This abstract class handles the Pumah 3D model rotation.
**/

public abstract class RotationDevice : MonoBehaviour, IRotationDevice {
    protected Pumah pumah;                                // Reference for Pumah model
    protected float errorTilt = 0, errorPan = 0;          // Angular error between the hand and the device
    protected float targetTilt = 0, targetPan = 0;        // Total angle applied to the pivot
    protected float currentTilt = 0, currentPan = 0;

    public virtual void Start() {

        if (this.pumah == null) {
            this.pumah = this.GetComponent<Pumah>();
        }
    }

    public virtual void Update()
    {
        if (this.pumah.rotationOn && !this.pumah.targetTooFar)
        {
            // Calculate the angular errors and corrections to be applied
            this.targetPan = this.getTotalErrorPanAngle();
            this.targetTilt = this.getTotalErrorTiltAngle();

            this.errorPan = this.targetPan - this.currentPan;
            this.errorTilt = this.targetTilt - this.currentTilt;

            if ((Math.Abs(this.errorPan) > 0.001) || (Math.Abs(this.errorTilt) > 0.001)) {
                this.updateDeviceAngles();
            }

        }
    }

    private void updateDeviceAngles()
    {
        // Reset the rotation to the starting position
        this.pumah.baseUltrahaptics.transform.rotation = Quaternion.LookRotation(this.pumah.proxyUltrahaptics.transform.forward , this.pumah.proxyUltrahaptics.transform.up);
        this.pumah.pivotUltrahaptics.transform.localRotation = Quaternion.identity;

        // Rotate the model
        this.pumah.baseUltrahaptics.transform.Rotate(0, this.targetPan, 0, Space.Self);
        this.pumah.pivotUltrahaptics.transform.Rotate(this.targetTilt, 0, 0, Space.Self);

        this.currentPan = this.targetPan;
        this.currentTilt = this.targetTilt;
    }

    // Get the total angular error between the hand and the device in default position for the tilt
    public abstract float getTotalErrorTiltAngle();

    // Get the total angular error between the hand and the device in default position for the pan
    public abstract float getTotalErrorPanAngle();
}
