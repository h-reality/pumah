﻿using System;
using System.Globalization;
using System.IO;
using UnityEngine;

/**
    05/03/2020 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    This file is part of PUMAH.

    PUMAH is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PUMAH is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PUMAH.  If not, see <https://www.gnu.org/licenses/>.

    This implementation is used to save and read position values in a .txt file.
**/

public class TextFileParser : MonoBehaviour, ICalibratorParser
{
    private string path;                                   // Path to the txt file containing the position and rotation values
    private string separator;                              // separator between values in same line
    private Quaternion sceneRotation;                      // Stored value of scene rotation angle
    private Vector3 positionValue;                         // First position taken for calibration

    private void Awake()
    {
        this.path = "Assets/Resources/calibration_vive.txt";
        this.separator = ";";

        this.getStoredValues();
    }

    // Load calibration values from .txt file
    public void getStoredValues()
    {
        StreamReader reader = new StreamReader(this.path);
        NumberFormatInfo nfi = new NumberFormatInfo();
        nfi.NegativeSign = "−";

        try
        {
            string[] values = reader.ReadLine().Split(this.separator.ToCharArray()[0]);
            this.positionValue = new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));
            this.sceneRotation = new Quaternion(float.Parse(values[3]), float.Parse(values[4]), float.Parse(values[5]), float.Parse(values[6]));
            Debug.Log("Position read : " + this.positionValue.ToString());
        }
        catch (Exception e)
        {
            Debug.LogError("Can't read values in file : " + e.Message);
            this.positionValue = new Vector3(0, 0, 0);
        }

        reader.Close();
    }

    // Save calibration's values in the .txt file
    public void saveValues() {
        StreamWriter writer = new StreamWriter(this.path, false);

        writer.WriteLine(this.positionValue.x + this.separator
                       + this.positionValue.y + this.separator
                       + this.positionValue.z + this.separator
                       + this.sceneRotation.x + this.separator
                       + this.sceneRotation.y + this.separator
                       + this.sceneRotation.z + this.separator
                       + this.sceneRotation.w);

        writer.Close();
        Debug.Log("Saved position and rotation values.");
    }

    public Quaternion getSceneRotation() {
        return this.sceneRotation;
    }

    public Vector3 getPosition() {
        return this.positionValue;
    }

    public void setPositionAndRotation(Vector3 position, Quaternion rotation) {
        this.positionValue = position;
        this.sceneRotation = rotation;
    }
}
