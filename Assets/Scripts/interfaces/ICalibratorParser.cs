﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
    05/03/2020 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    This file is part of PUMAH.

    PUMAH is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PUMAH is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PUMAH.  If not, see <https://www.gnu.org/licenses/>.

    This interface is the common implementation for parsing and saving position values of the PUMAH.
**/

public interface ICalibratorParser
{
    void getStoredValues();
    void saveValues();
    Quaternion getSceneRotation();
    Vector3 getPosition();
    void setPositionAndRotation(Vector3 position, Quaternion rotation);
}
