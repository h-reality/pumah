﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
    05/03/2020 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    This file is part of PUMAH.

    PUMAH is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PUMAH is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PUMAH.  If not, see <https://www.gnu.org/licenses/>.

    This core class handles the Pumah prefab.
**/

[RequireComponent(typeof(IRotationDevice))]
[RequireComponent(typeof(ICalibratorParser))]
public class Pumah : MonoBehaviour {

    [Tooltip("Pivot point of the Pumah (Middle of the support plate).")]
    [HideInInspector]
    public GameObject pivotUltrahaptics;
    [Tooltip("The center point of the Pumah base.")]
    [HideInInspector]
    public GameObject baseUltrahaptics;
    [Tooltip("UltraHaptics Prefab.")]
    [HideInInspector]
    public GameObject ultrahaptics;
    [Tooltip("Needed for angular error calculation.")]
    [HideInInspector]
    public GameObject proxyUltrahaptics;
    [Tooltip("Pivot point for position and orientation of the scene. The scene will be located and rotated according to this point. This is the center of your haptics sensations.")]
    public GameObject scenePivotPoint;
    [Tooltip("Script to rotate the pan & tilt axes of the Pumah.")]
    [HideInInspector]
    public IRotationDevice rotationDevice;
    [Tooltip("Script to read and store position and rotation values of calibration.")]
    public ICalibratorParser parser;
    [Tooltip("The GameObject that the Pumah is tracking.")]
    public GameObject target;
    [Tooltip("Display / Hide the virtual Pumah model.")]
    public bool hideDeviceInScene = false;
    [Tooltip("Enable / Disable tracking rotation for the PUMAH.")]
    public bool rotationOn;
    [Tooltip("Prevent PUMAH from rotating if the tracked target gets too far from it.")]
    public bool targetTooFar = false;
    [Tooltip("The distance (in m) at which the Pumah stops tracking the target.")]
    public float maxDistanceTarget = 0.5f;
    [Tooltip("Vive tracker / forward vector angle offset.")]
    public float trackerAngleOffset = 180;
    [Tooltip("Vive tracker horizontal offset with Ultrahaptics base.")]
    public float trackerHorizontalOffset = 0.106f;
    [Tooltip("Vive tracker vertical offset with Ultrahaptics base.")]
    public float trackerVerticalOffset = 0.092f;

    private void Awake() {
        if (this.rotationDevice == null) {
            this.rotationDevice = this.GetComponent<IRotationDevice>();

            if (this.rotationDevice == null) {
                Debug.LogError("The Pumah has no rotation device. Add a script implementing IRotationDevice to this GameObject.");
            }
        }

        if (this.parser == null) {
            this.parser = this.GetComponent<ICalibratorParser>();

            if (this.parser == null) {
                Debug.LogError("The Pumah has no parser to retrieve position values. Add a script implementing ICalibratorParser to this GameObject.");
            }
        }

        if (this.baseUltrahaptics == null) {
            this.baseUltrahaptics = this.transform.Find("Base_Ultrahaptics").gameObject;
        }
        if (this.proxyUltrahaptics == null) {
            this.proxyUltrahaptics = this.transform.Find("Base_Proxy_Ultrahaptics").gameObject;
        }
        if (this.pivotUltrahaptics == null) {
            this.pivotUltrahaptics = this.baseUltrahaptics.transform.Find("Pivot_Ultrahaptics").gameObject;
        }
        if (this.ultrahaptics == null) {
            this.ultrahaptics = this.pivotUltrahaptics.transform.Find("UltrahapticsKit").gameObject;
        }
    }

    private void Start() {
        if (this.hideDeviceInScene) {
            // Hide the ultrahaptics device in the scene
            this.baseUltrahaptics.GetComponent<Renderer>().enabled = false;
            this.pivotUltrahaptics.GetComponent<Renderer>().enabled = false;
            this.ultrahaptics.transform.GetChild(0).gameObject.GetComponent<Renderer>().enabled = false;
            this.ultrahaptics.transform.GetChild(1).gameObject.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            this.ultrahaptics.transform.GetChild(2).gameObject.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            for (int i = 0; i < this.baseUltrahaptics.transform.childCount; i++) {
                this.baseUltrahaptics.transform.GetChild(i).gameObject.GetComponent<Renderer>().enabled = false;
            }
        }

        this.UpdateArrayLocation();
    }

    private void Update() {
        //Get the distance between tracked controller and UH pivot point
        float dist = Vector3.Distance(this.pivotUltrahaptics.transform.position, this.target.transform.position);
        //If distance is too high, stop rotating the PUMAH
        this.targetTooFar = (dist > this.maxDistanceTarget) ? true : false;
    }

    // Update the position and rotation of the objects with world space values
    public void UpdateArrayLocation() {
        this.baseUltrahaptics.transform.position = this.parser.getPosition();
        this.proxyUltrahaptics.transform.position = this.parser.getPosition();
        this.pivotUltrahaptics.transform.position = this.parser.getPosition() + new Vector3(0, 0.144f, 0);
        this.ultrahaptics.transform.position = this.parser.getPosition() + new Vector3(0, 0.147f, 0);

        this.baseUltrahaptics.transform.rotation = this.parser.getSceneRotation();
        this.ultrahaptics.transform.rotation = this.parser.getSceneRotation();
        this.proxyUltrahaptics.transform.rotation = this.parser.getSceneRotation();

        this.scenePivotPoint.transform.position = this.parser.getPosition() - new Vector3(0, this.trackerVerticalOffset, 0);
        this.scenePivotPoint.transform.rotation = this.parser.getSceneRotation();
    }
}
