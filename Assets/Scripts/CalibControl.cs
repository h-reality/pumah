﻿using System;
using System.IO;
using UnityEngine;
using Valve.VR;

/**
    05/03/2020 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    This file is part of PUMAH.

    PUMAH is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PUMAH is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PUMAH.  If not, see <https://www.gnu.org/licenses/>.

    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

public class CalibControl : MonoBehaviour
{
    public SteamVR_Action_Boolean triggerPress = null;     // Trigger press event (to sample the position)
    public SteamVR_Action_Boolean graspPress = null;       // Grasp press event (to save values in the .csv file)
    public SteamVR_Behaviour_Pose controller = null;       // The physical controller

    public GameObject pointCalib;                          // Point of calibration (sphere attached to the Vive controller)
    public Pumah pumah;                                    // Reference for Pumah model

    private Quaternion sceneRotation;

    private void Start() {
        if (this.controller == null) {
            this.controller = this.GetComponent<SteamVR_Behaviour_Pose>();
        }

        //Set the calibration sphere to the Pumah's pivot point
        this.pointCalib.transform.position = new Vector3(0, this.pumah.trackerVerticalOffset, this.pumah.trackerHorizontalOffset);
    }

    private void Update()
    {
        if (this.triggerPress.GetStateDown(this.controller.inputSource)) {
            //If trigger is pressed on controller,
            //register the UH position and locate the model
            Quaternion rotation = Quaternion.LookRotation(this.pointCalib.transform.forward);
            this.sceneRotation = new Quaternion(0, rotation.y, 0, rotation.w) * Quaternion.Euler(Vector3.up * this.pumah.trackerAngleOffset);
            this.pumah.parser.setPositionAndRotation(this.pointCalib.transform.position, this.sceneRotation);
            Debug.Log("Position sampled");

            this.pumah.UpdateArrayLocation();

        } else if (this.graspPress.GetStateDown(this.controller.inputSource)) {
            //If grasp button is pressed on controller,
            //save values in the .txt file
            try
            {
                this.pumah.parser.saveValues();
            }
            catch (Exception e)
            {
                Debug.LogError("Error when writing in the saved file : " + e.Message);
            }
        }
    }
}
