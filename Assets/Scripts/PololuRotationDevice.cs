﻿using System.Collections.Generic;
using UnityEngine;
using Pololu.UsbWrapper;
using Pololu.Usc;
using System;

/**
    05/03/2020 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    This file is part of PUMAH.

    PUMAH is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PUMAH is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PUMAH.  If not, see <https://www.gnu.org/licenses/>.

    This implementation is used for rotating the Pumah with a couple of servo-motors.
    It translates a rotation angle value to a PWM value to send to the motors.
**/

public class PololuRotationDevice : RotationDevice
{
    //PWM values for given positions
    //////////////////////////////////////////////////////
    // PAN
    [HideInInspector] public int MIN_PAN;
    [HideInInspector] public int MID_PAN;
    [HideInInspector] public int MAX_PAN;

    // TILT
    [HideInInspector] public int MIN_TILT;
    [HideInInspector] public int MIN_MID_TILT;
    [HideInInspector] public int MID_TILT;
    [HideInInspector] public int MAX_MID_TILT;
    [HideInInspector] public int MAX_TILT;
    // Needed if the total angular travel produced by servos > 180°
    [HideInInspector] public bool bottomMinTiltEnabled = true;
    [HideInInspector] public int BOTTOM_MIN_TILT;
    [HideInInspector] public int BOTTOM_MIN_ANGLE;
    [HideInInspector] public bool bottomMaxTiltEnabled = true;
    [HideInInspector] public int BOTTOM_MAX_TILT;
    [HideInInspector] public int BOTTOM_MAX_ANGLE;
    //////////////////////////////////////////////////////

    private ushort currentTiltPWM, currentPanPWM;               // PWM values for the servos
    private bool panBlocked = false;                            // If the tilt is near 0°, blocks the pan movement to middle travel
    private int previousTilt;                                   // Precedent value of the tilt
    private int previousPan;                                    // Precedent value of the pan
    private Usc device;                                         // Serial communicator
    [SerializeField] private float totalTiltAngle = 260.0f;     // Total angular course allowed by servo-motors

    public override void Start()
    {
        base.Start();
        //Connect to pololu
        this.device = this.ConnectToDevice();
        // Set the device to center position
        this.RevertToBasePosition();
    }

    public override void Update()
    {
        base.Update();

        if (this.pumah.rotationOn) {
            if (this.targetTilt > this.BOTTOM_MIN_ANGLE) {
                this.targetTilt = this.BOTTOM_MIN_ANGLE;

            } else if (this.targetTilt < this.BOTTOM_MAX_ANGLE) {
                this.targetTilt = this.BOTTOM_MAX_ANGLE;
            }

            if (this.targetPan > 90.0f) {
                this.targetPan = 90.0f;
            } else if (this.targetPan < -90.0f) {
                this.targetPan = -90.0f;
            }

            if (this.targetTilt > -15.0f && this.targetTilt < 15.0f) {
                this.panBlocked = true;
            } else {
                this.panBlocked = false;
            }

            this.currentTiltPWM = this.getCurrentTiltAnglePWM();
            this.currentPanPWM = this.getCurrentPanAnglePWM();

            if (this.previousTilt != this.currentTiltPWM) {
                this.previousTilt = this.currentTiltPWM;
                this.TrySetTarget(0x00, Convert.ToUInt16(this.currentTiltPWM * 4));
            }

            if (this.previousPan != this.currentPanPWM) {
                this.previousPan = this.currentPanPWM;
                this.TrySetTarget(0x01, Convert.ToUInt16(this.currentPanPWM * 4));
            }

        }
    }

    // Get the total angle error between the hand and the device in default position for the tilt
    public override float getTotalErrorTiltAngle() {
        Vector3 vectorDeviceTarget = (this.pumah.target.transform.position - this.pumah.pivotUltrahaptics.transform.position);
        Plane planeZY = new Plane(this.pumah.baseUltrahaptics.transform.right, this.pumah.baseUltrahaptics.transform.position);
        Vector3 vectorDeviceTargetOnPlane = Vector3.ProjectOnPlane(vectorDeviceTarget, planeZY.normal);

        this.errorTilt = Vector3.SignedAngle(vectorDeviceTargetOnPlane, this.pumah.baseUltrahaptics.transform.up, this.pumah.baseUltrahaptics.transform.right);

        if (this.errorTilt > this.getMinTiltAngle()) {
            this.targetTilt = this.getMinTiltAngle();

        } else if (this.errorTilt < this.getMaxTiltAngle()) {
            this.targetTilt = this.getMaxTiltAngle();

        } else {
            this.targetTilt = this.errorTilt;
        }

        return this.targetTilt;
    }

    // Get the total angle error between the hand and the device in default position for the pan
    public override float getTotalErrorPanAngle() {
        Vector3 vectorDeviceTarget = (this.pumah.target.transform.position - this.pumah.pivotUltrahaptics.transform.position);
        Plane planeZX = new Plane(this.pumah.proxyUltrahaptics.transform.up, this.pumah.proxyUltrahaptics.transform.position);
        Vector3 vectorDeviceTargetOnPlane = Vector3.ProjectOnPlane(vectorDeviceTarget, planeZX.normal);

        this.errorPan = Vector3.SignedAngle(this.pumah.proxyUltrahaptics.transform.forward, vectorDeviceTargetOnPlane, this.pumah.proxyUltrahaptics.transform.up);

        if (this.errorPan > 90.0f) {
            this.targetPan = this.errorPan - 180.0f;

        } else if (this.errorPan < -90.0f) {
            this.targetPan = this.errorPan + 180.0f;

        } else {
            this.targetPan = this.errorPan;
        }

        return this.targetPan;
    }

    private ushort getCurrentPanAnglePWM() {
        if (!this.panBlocked) {
            if (Mathf.RoundToInt(this.targetPan) == -90) {
                return (ushort)this.MIN_PAN;

            } else if (this.targetPan > -90 && this.targetPan < 0) {
                return (ushort)this.linterp(this.targetPan, -90f, 0f, this.MIN_PAN, this.MID_PAN);

            } else if (Mathf.RoundToInt(this.targetPan) == 0) {
                return (ushort)this.MID_PAN;

            } else if (this.targetPan > 0 && this.targetPan < 90) {
                return (ushort)this.linterp(this.targetPan, 0f, 90f, this.MID_PAN, this.MAX_PAN);

            } else if (Mathf.RoundToInt(this.targetPan) == 90) {
                return (ushort)this.MAX_PAN;
            }
        }

        return (ushort)this.MID_PAN;
    }

    private ushort getCurrentTiltAnglePWM() {
        if (Mathf.RoundToInt(this.targetTilt) == this.BOTTOM_MAX_ANGLE && this.bottomMaxTiltEnabled) {
            return (ushort)this.BOTTOM_MAX_TILT;

        } else if (this.targetTilt > this.BOTTOM_MAX_ANGLE && this.targetTilt < -90 && this.bottomMaxTiltEnabled) {
            return (ushort)this.linterp(this.targetTilt, this.BOTTOM_MAX_ANGLE, -90f, this.BOTTOM_MAX_TILT, this.MAX_TILT);

        } else if (Mathf.RoundToInt(this.targetTilt) == -90) {
            return (ushort)this.MAX_TILT;

        } else if (this.targetTilt > -90 && this.targetTilt < -45) {
            return (ushort)this.linterp(this.targetTilt, -90f, -45f, this.MAX_TILT, this.MAX_MID_TILT);

        } else if (Mathf.RoundToInt(this.targetTilt) == -45) {
            return (ushort)this.MAX_MID_TILT;

        } else if (this.targetTilt > -45 && this.targetTilt < 0) {
            return (ushort)this.linterp(this.targetTilt, -45f, 0f, this.MAX_MID_TILT, this.MID_TILT);

        } else if (Mathf.RoundToInt(this.targetTilt) == 0) {
            return (ushort)this.MID_TILT;

        } else if (this.targetTilt > 0 && this.targetTilt < 45) {
            return (ushort)this.linterp(this.targetTilt, 0f, 45f, this.MID_TILT, this.MIN_MID_TILT);

        } else if (Mathf.RoundToInt(this.targetTilt) == 45) {
            return (ushort)this.MIN_MID_TILT;

        } else if (this.targetTilt > 45 && this.targetTilt < 90) {
            return (ushort)this.linterp(this.targetTilt, 45f, 90f, this.MIN_MID_TILT, this.MIN_TILT);

        } else if (Mathf.RoundToInt(this.targetTilt) == 90) {
            return (ushort)this.MIN_TILT;

        } else if (this.targetTilt > 90 && this.targetTilt < this.BOTTOM_MIN_ANGLE && this.bottomMinTiltEnabled) {
            return (ushort)this.linterp(this.targetTilt, 90f, this.BOTTOM_MIN_ANGLE, this.MIN_TILT, this.BOTTOM_MIN_TILT);

        } else if (Mathf.RoundToInt(this.targetTilt) == this.BOTTOM_MIN_ANGLE && this.bottomMinTiltEnabled) {
            return (ushort)this.BOTTOM_MIN_TILT;
        }

        return (ushort)this.MID_TILT;
    }

    private void OnApplicationQuit()
    {
        this.DisconnectFromDevice();
    }

    // Revert the position of the tilt and pan motor to the base position
    private void RevertToBasePosition()
    {
        this.currentTiltPWM = (ushort)this.MID_TILT;
        this.targetTilt = 0;
        this.previousTilt = this.currentTiltPWM;

        this.currentPanPWM = (ushort)this.MID_PAN;
        this.targetPan = 0;
        this.previousPan = this.currentPanPWM;

        this.TrySetTarget(0x00, Convert.ToUInt16(this.currentTiltPWM * 4));
        this.TrySetTarget(0x01, Convert.ToUInt16(this.currentPanPWM * 4));
    }

    private void TrySetTarget(byte channel, ushort target)
    {
        try
        {
            this.device.setTarget(channel, target);
        }
        catch
        {
        }
    }    
    
    private Usc ConnectToDevice()
    {
        // Get a list of all connected devices of this type.
        List<DeviceListItem> connectedDevices = Usc.getConnectedDevices();

        foreach (DeviceListItem dli in connectedDevices)
        {    
            Usc device = new Usc(dli);    // Connect to the device.
            return device;                // Return the device.
        }

        Debug.LogError("Could not find device.  Make sure it is plugged in to USB " +
            "and check your Device Manager (Windows) or run lsusb (Linux).");

        return null;
    }

    private void DisconnectFromDevice()
    {
        try
        {
            this.RevertToBasePosition();

            Int32 i = 0;
            Int32 max = 999999999 / 3;

            while (i < max)
                i++;

            this.currentTiltPWM = Convert.ToUInt16(this.MAX_TILT);

            this.TrySetTarget(0x01, 0);
            this.TrySetTarget(0x00, 0);

            this.device.Dispose();
        }
        catch
        {
        }
    }

    private int linterp(float currentAngle, float minAngle, float maxAngle, int musMin, int musMax) {
        return Convert.ToUInt16(musMin + (((currentAngle - minAngle) / (maxAngle - minAngle)) * (musMax - musMin)));
    }

    private int getMinTiltAngle() {
        if (this.bottomMinTiltEnabled) {
            return this.BOTTOM_MIN_ANGLE;
        } else {
            return 90;
        }
    }

    private int getMaxTiltAngle() {
        if (this.bottomMaxTiltEnabled) {
            return this.BOTTOM_MAX_ANGLE;
        } else {
            return -90;
        }
    }
}
