﻿using UnityEditor;
using UnityEngine;

/**
    05/03/2020 | Author : Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr

    This file is part of PUMAH.

    PUMAH is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PUMAH is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PUMAH.  If not, see <https://www.gnu.org/licenses/>.

    This class handles the editor view of the PololuRotationDevice script.
**/


[CustomEditor(typeof(PololuRotationDevice))]
[System.Serializable]
public class PololuRotationDeviceEditor : Editor {
    SerializedProperty minPan;
    SerializedProperty midPan;
    SerializedProperty maxPan;

    SerializedProperty minTilt;
    SerializedProperty minMidTilt;
    SerializedProperty midTilt;
    SerializedProperty maxMidTilt;
    SerializedProperty maxTilt;
    SerializedProperty botMinTiltEnabled;
    SerializedProperty botMinTilt;
    SerializedProperty botMinAngle;
    SerializedProperty botMaxTiltEnabled;
    SerializedProperty botMaxTilt;
    SerializedProperty botMaxAngle;
    SerializedProperty totalTiltAngle;

    private static bool showTiltValues = true;
    private static bool showPanValues = true;

    protected virtual void OnEnable() {
        this.minPan = this.serializedObject.FindProperty("MIN_PAN");
        this.midPan = this.serializedObject.FindProperty("MID_PAN");
        this.maxPan = this.serializedObject.FindProperty("MAX_PAN");

        this.minTilt = this.serializedObject.FindProperty("MIN_TILT");
        this.minMidTilt = this.serializedObject.FindProperty("MIN_MID_TILT");
        this.midTilt = this.serializedObject.FindProperty("MID_TILT");
        this.maxMidTilt = this.serializedObject.FindProperty("MAX_MID_TILT");
        this.maxTilt = this.serializedObject.FindProperty("MAX_TILT");
        this.botMinTiltEnabled = this.serializedObject.FindProperty("bottomMinTiltEnabled");
        this.botMinTilt = this.serializedObject.FindProperty("BOTTOM_MIN_TILT");
        this.botMinAngle = this.serializedObject.FindProperty("BOTTOM_MIN_ANGLE");
        this.botMaxTiltEnabled = this.serializedObject.FindProperty("bottomMaxTiltEnabled");
        this.botMaxTilt = this.serializedObject.FindProperty("BOTTOM_MAX_TILT");
        this.botMaxAngle = this.serializedObject.FindProperty("BOTTOM_MAX_ANGLE");
        this.totalTiltAngle = this.serializedObject.FindProperty("totalTiltAngle");
    }

    public override void OnInspectorGUI() {
        this.serializedObject.Update();

        GUIStyle format = new GUIStyle();
        format.fontSize = 10;
        format.alignment = TextAnchor.MiddleCenter;
        format.stretchHeight = true;

        GUILayoutOption[] options = { GUILayout.MaxWidth(36.0f), GUILayout.MinWidth(36.0f) };
        GUILayoutOption[] labelOptions = { GUILayout.MaxWidth(44.0f), GUILayout.MinWidth(44.0f) };

        showTiltValues = EditorGUILayout.Foldout(showTiltValues, "TILT PWM VALUES");

        if (showTiltValues)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.BeginVertical();
                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("-45°", format, labelOptions);
                            this.maxMidTilt.intValue = EditorGUILayout.IntField(this.maxMidTilt.intValue, options);
                        EditorGUILayout.EndHorizontal();

                        GUILayout.Space(30);

                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("-90°", format, labelOptions);
                            this.maxTilt.intValue = EditorGUILayout.IntField(this.maxTilt.intValue, options);
                        EditorGUILayout.EndHorizontal();

                        GUILayout.Space(30);

                        EditorGUILayout.BeginHorizontal();
                            this.botMaxTiltEnabled.boolValue = EditorGUILayout.ToggleLeft(this.botMaxAngle.intValue.ToString() + "°",
                                                                                          this.botMaxTiltEnabled.boolValue, format, labelOptions);
                            this.botMaxTilt.intValue = EditorGUILayout.IntField(this.botMaxTilt.intValue, options);
                        EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndVertical();

                    GUILayout.FlexibleSpace();

                    EditorGUILayout.BeginVertical();

                        EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            EditorGUILayout.LabelField("0°", format, labelOptions);
                                this.midTilt.intValue = EditorGUILayout.IntField(this.midTilt.intValue, options);
                            GUILayout.FlexibleSpace();
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            Texture2D pumahTilt = Resources.Load<Texture2D>("pumah_tilt");
                            GUILayout.Label(pumahTilt, GUILayout.Width(84.0f), GUILayout.Height(84.0f));
                            GUILayout.FlexibleSpace();
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            this.totalTiltAngle.floatValue = GUILayout.HorizontalSlider(this.totalTiltAngle.floatValue, 180.0f, 350.0f, GUILayout.Width(84.0f));
                            GUILayout.FlexibleSpace();
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            EditorGUILayout.LabelField("Total Tilt angle : " + ((int)this.totalTiltAngle.floatValue).ToString() + "°", format, options);
                            GUILayout.FlexibleSpace();
                        EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndVertical();

                    GUILayout.FlexibleSpace();

                    EditorGUILayout.BeginVertical();
                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("45°", format, labelOptions);
                            this.minMidTilt.intValue = EditorGUILayout.IntField(this.minMidTilt.intValue, options);
                        EditorGUILayout.EndHorizontal();
                        GUILayout.Space(30);
                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("90°", format, labelOptions);
                            this.minTilt.intValue = EditorGUILayout.IntField(this.minTilt.intValue, options);
                        EditorGUILayout.EndHorizontal();
                        GUILayout.Space(30);
                        EditorGUILayout.BeginHorizontal();
                            this.botMinTiltEnabled.boolValue = EditorGUILayout.ToggleLeft(this.botMinAngle.intValue.ToString() + "°",
                                                                                          this.botMinTiltEnabled.boolValue, format, labelOptions);
                            this.botMinTilt.intValue = EditorGUILayout.IntField(this.botMinTilt.intValue, options);
                        EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();

                EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

        }

        if (this.totalTiltAngle.floatValue == 180.0f) {
            this.botMaxTiltEnabled.boolValue = false;
            this.botMinTiltEnabled.boolValue = false;
        } else {
            this.botMaxTiltEnabled.boolValue = true;
            this.botMinTiltEnabled.boolValue = true;
        }

        this.botMinAngle.intValue = Mathf.RoundToInt(this.totalTiltAngle.floatValue / 2);
        this.botMaxAngle.intValue = - Mathf.RoundToInt(this.totalTiltAngle.floatValue / 2);

        showPanValues = EditorGUILayout.Foldout(showPanValues, "PAN PWM VALUES", true);

        if (showPanValues)
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);

                EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.BeginVertical();
                        GUILayout.Space(30);
                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("-90°", format, labelOptions);
                            this.minPan.intValue = EditorGUILayout.IntField(this.minPan.intValue, options);
                        EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();

                    GUILayout.FlexibleSpace();

                    EditorGUILayout.BeginVertical();
                        EditorGUILayout.BeginHorizontal();
                            GUILayout.Space(10);
                            Texture2D pumahPan = Resources.Load<Texture2D>("pumah_pan");
                            GUILayout.Label(pumahPan, GUILayout.Width(84.0f), GUILayout.Height(84.0f));
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("0°", format, labelOptions);
                            this.midPan.intValue = EditorGUILayout.IntField(this.midPan.intValue, options);
                        EditorGUILayout.EndHorizontal();
                        GUILayout.Space(5);
                    EditorGUILayout.EndVertical();

                    GUILayout.FlexibleSpace();

                    EditorGUILayout.BeginVertical();
                        GUILayout.Space(30);
                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("90°", format, labelOptions);
                            this.maxPan.intValue = EditorGUILayout.IntField(this.maxPan.intValue, options);
                        EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();

                EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        this.serializedObject.ApplyModifiedProperties();
    }
}
